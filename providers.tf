terraform {
  backend "s3" {
   bucket         = "tf-simple-instance-mybucket"
   key            = "state/terraform.tfstate"
   region         = "eu-west-3"
   dynamodb_table = "tf-simple-instance"
 }

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.57.0"
    }
    tls = {
      source = "hashicorp/tls"
      version = "4.0.4"
    }
    local = {
      source = "hashicorp/local"
      version = "2.3.0"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-west-3"
}
